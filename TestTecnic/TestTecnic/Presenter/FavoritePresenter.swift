//
//  FavoritePresenter.swift
//  TestTecnic
//
//  Created by Thiago Rodrigues de Souza on 22/06/2018.
//  Copyright © 2018 Thiago Rodrigues de Souza. All rights reserved.
//

import Foundation

struct FavoriteItem {
	let title: String
	let description: String
	let imageUrl: String
}

protocol FavoritesView {
	func startLoading()
	func finishLoading()
	func favoriteIsEmpty()
	func loadingFavorites(favoritedItens: Array<FavoriteItem>)
	func showFavoriteItemDetail(favoriteItem: FavoriteItem)
}

class FavoritePresenter {
	
	private let categoryService: CategoryService
	private var favoriteView: FavoritesView?
	
	init(categorieService: CategoryService) {
		self.categoryService = categorieService
	}
	
	func attachView(view: FavoritesView){
		favoriteView = view
	}
	
	func detachView() {
		favoriteView = nil
	}
	
	func setup(){
		self.favoriteView?.startLoading()
		let dataItem = categoryService.listItem()
		let favoritedItens = dataItem.compactMap{
			FavoriteItem(title: $0.title! ,
						 description: $0.item_description! ,
						 imageUrl: $0.imageUrl!)
		}
		if favoritedItens.isEmpty{
			self.favoriteView?.favoriteIsEmpty()
		}else{
			self.favoriteView?.loadingFavorites(favoritedItens: favoritedItens)
			self.favoriteView?.finishLoading()
		}
	}
	
	func selectItem(item: FavoriteItem) {
		self.favoriteView?.showFavoriteItemDetail(favoriteItem: item)
	}
	
}
