//
//  DetaillPresenter.swift
//  TestTecnic
//
//  Created by Thiago Rodrigues de Souza on 22/06/2018.
//  Copyright © 2018 Thiago Rodrigues de Souza. All rights reserved.
//

import Foundation

protocol DetailView {
	func startLoading()
	func finishLoading()
	func setItem(item: ItemView)
	func renderStatus(status: Bool)
}

class DetaillPresenter{
	
	private let categoryService: CategoryService
	private var detailView: DetailView?

	init(service: CategoryService) {
		self.categoryService = service
	}
	
	func attachView(view: DetailView){
		detailView = view
	}
	
	func detachView() {
		detailView = nil
	}
	
	func setup(item: ItemView){
		self.detailView?.startLoading()
		self.detailView?.setItem(item: item)
		self.detailView?.finishLoading()
	}
	
	func favorite(item: ItemView){
		detailView?.startLoading()
		let status = categoryService.persistItem(title: item.title, description: item.description,
									imageUrl: item.imagesUrl[item.selecetedImage])
		detailView?.renderStatus(status: status)
		detailView?.finishLoading()
	}
	
	
	
}
