//
//  CategoryPresenter.swift
//  TestTecnic
//
//  Created by Thiago Rodrigues de Souza on 19/06/2018.
//  Copyright © 2018 Thiago Rodrigues de Souza. All rights reserved.
//
import Foundation

protocol MainView {
	func startLoading()
	func finishLoading()
	func showMessageError()
	func setCategories(categories: [CatergoryView])
	func updateImage(indexPath: IndexPath, url: String, positionImage: Int)
	func showItemDetail(item: ItemView)
}

enum TouchDirection {
	case left, right
}

class CategoryPresenter {
	private let categoryService: CategoryService
	private var mainView: MainView?
	
	init(service: CategoryService) {
		self.categoryService = service
	}
	
	func attachView(view: MainView){
		mainView = view
	}
	
	func detachView() {
		mainView = nil
	}
	
	func replaceImage(index: IndexPath, direction: TouchDirection, item: ItemView) {
		let lastIndexItemArray = (item.imagesUrl.count - 1)
		let currentPosition = item.selecetedImage
		switch direction {
		case .left:
			let newImagePosition = self.returnNewPosition(currentPosition: currentPosition, offset: -1, lastIndex: lastIndexItemArray)
			let newUrl = item.imagesUrl[newImagePosition]
			self.mainView?.updateImage(indexPath: index, url: newUrl, positionImage: newImagePosition)
			break
		case .right:
			let newImagePosition = self.returnNewPosition(currentPosition: currentPosition,
														  offset: 1, lastIndex: lastIndexItemArray)
			let newUrl = item.imagesUrl[newImagePosition]
			self.mainView?.updateImage(indexPath: index, url: newUrl, positionImage: newImagePosition)
			break
		}
	}
	
	fileprivate func returnNewPosition(currentPosition: Int, offset: Int, lastIndex: Int) -> Int{
		let amountPosition = currentPosition + offset
		switch amountPosition {
		case _ where amountPosition > lastIndex:
			return 0
		case _ where amountPosition < 0:
			return lastIndex
		default:
			return amountPosition
		}
	}
	
	func setupView(){
		self.mainView?.startLoading()
		CategoryService().listCategories { (categories, error) in
			if(error == nil){
				let categoriesView = categories.compactMap({ (category) -> CatergoryView in
					return CatergoryView(nameCategory: category.name,
										 items: category.items.compactMap({ (item) -> ItemView in
											return ItemView(title: item.title, description: item.description,
															imagesUrl: item.galery, selecetedImage: 0)
										}))
				})
				self.mainView?.finishLoading()
				self.mainView?.setCategories(categories: categoriesView)
			}else{
				self.mainView?.finishLoading()
				self.mainView?.showMessageError()
			}
		}
	}
	
	func selectItem(item: ItemView) {
		self.mainView?.showItemDetail(item: item)
	}
}
