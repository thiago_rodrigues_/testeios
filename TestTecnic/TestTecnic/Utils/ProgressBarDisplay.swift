//
//  ProgressBarDisplay.swift
//  TestTecnic
//
//  Created by Thiago Rodrigues de Souza on 22/06/2018.
//  Copyright © 2018 Thiago Rodrigues de Souza. All rights reserved.
//
import UIKit

class ProgressBarDisplay {
	
	fileprivate var messageFrame = UIView()
	fileprivate var activityIndicator = UIActivityIndicatorView()
	fileprivate var strLabel = UILabel()
	fileprivate var isLoading = false
	
	init(){
		messageFrame.layer.cornerRadius = 15
		messageFrame.backgroundColor = UIColor(white: 0, alpha: 0.7)
		activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.white)
		strLabel = UILabel(frame: CGRect(x: 50, y: 0, width: 200, height: 50))
	}
	
	func show(message: String, indicator: Bool, in view: UIView){
		messageFrame.frame =  CGRect(x: 0, y: 0 , width: 180, height: 50)
		messageFrame.center = view.center
		if indicator{
			activityIndicator.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
			activityIndicator.startAnimating()
			messageFrame.addSubview(activityIndicator)
		}
		if !isLoading{
			isLoading = true
			strLabel.text = message
			strLabel.textColor = UIColor.white
			messageFrame.addSubview(strLabel)
			view.addSubview(messageFrame)
		}
		if messageFrame.subviews.contains(strLabel){
			view.addSubview(messageFrame)
		}
	}
	
	func hide(){
		isLoading = false
		DispatchQueue.main.async {
			self.messageFrame.removeFromSuperview()
		}
	}
	
}

