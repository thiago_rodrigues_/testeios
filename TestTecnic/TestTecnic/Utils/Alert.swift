//
//  Alert.swift
//  TestTecnic
//
//  Created by Thiago Rodrigues de Souza on 22/06/2018.
//  Copyright © 2018 Thiago Rodrigues de Souza. All rights reserved.
//

import UIKit

class Alert{
	
	class func showDefault(title: String?, message: String?, in viewController: UIViewController, confirmActionHandler: ((_ action: UIAlertAction)->Void)?, completion: (()->Void)?){
		let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
		let confirmAction = UIAlertAction(title: "OK", style: .default, handler: confirmActionHandler)
		alert.addAction(confirmAction)
		DispatchQueue.main.async{
			alert.dismiss(animated: true, completion: nil)
			viewController.present(alert, animated: true, completion: completion)
		}
	}
	
	class func show(with title: String?, message: String?, in viewController: UIViewController, confirmActionHandler: ((_ action: UIAlertAction)->Void)?, cancelAction: ((_ action: UIAlertAction)->Void)?, completion: (()->Void)?){
		let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
		let confirmAction = UIAlertAction(title: "Sim", style: .destructive, handler: confirmActionHandler)
		let cancelAction = UIAlertAction(title: "Não", style: .default, handler: cancelAction)
		alert.addAction(confirmAction)
		alert.addAction(cancelAction)
		DispatchQueue.main.async{
			viewController.present(alert, animated: true, completion: completion)
		}
	}
	
}
