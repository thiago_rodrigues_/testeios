//
//  Model.swift
//  TestTecnic
//
//  Created by Thiago Rodrigues de Souza on 19/06/2018.
//  Copyright © 2018 Thiago Rodrigues de Souza. All rights reserved.
//

struct Category {
	let name: String
	let items: Array<Item>
	
	init(dict: Dictionary<String, Any>){
		name =  dict["category"] as! String
		let arrayItem = dict["items"] as! Array<Dictionary<String, Any>>
		items = arrayItem.compactMap({Item(dict: $0)})
	}
}

struct Item {
	let title: String
	let description: String
	let galery: Array<String>

	init(dict: Dictionary<String, Any>){
		title = dict["title"] as! String
		description = dict["description"] as! String
		galery = dict["galery"] as! Array<String>
	}
}


