//
//  DetailFavoriteViewController.swift
//  TestTecnic
//
//  Created by Thiago Rodrigues de Souza on 22/06/2018.
//  Copyright © 2018 Thiago Rodrigues de Souza. All rights reserved.
//

import UIKit

class DetailFavoriteViewController: UIViewController {
	
	var itemFavorited: FavoriteItem?
	
	@IBOutlet weak var favoriteImage: UIImageView!
	@IBOutlet weak var favoriteTitle: UILabel!
	@IBOutlet weak var favoritedDescription: UITextView!
	
    override func viewDidLoad() {
        super.viewDidLoad()
		favoriteTitle.text = itemFavorited?.title ?? ""
		favoritedDescription.text = itemFavorited?.description ?? ""
		favoritedDescription.text = itemFavorited?.description ?? ""
	}
	
	@IBAction func back(_ sender: UIButton) {
		self.dismiss(animated: true, completion: nil)
	}
}
