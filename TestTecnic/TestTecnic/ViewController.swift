//
//  ViewController.swift
//  TestTecnic
//
//  Created by Thiago Rodrigues de Souza on 19/06/2018.
//  Copyright © 2018 Thiago Rodrigues de Souza. All rights reserved.
//
import UIKit
import SDWebImage

class ViewController: UIViewController {
	
	@IBOutlet weak var tableView: UITableView!
	private let categoryPresenter = CategoryPresenter(service: CategoryService())
	private var categories = Array<CatergoryView>()
	let progressBar = ProgressBarDisplay()

	override func viewDidLoad() {
		super.viewDidLoad()
		tableView.dataSource = self
		tableView.delegate = self
		categoryPresenter.attachView(view: self)
		categoryPresenter.setupView()
	}
}

extension ViewController: MainView {
	
	func startLoading() {
		progressBar.show(message: "Carregando...", indicator: true, in: view)
	}
	
	func finishLoading() {
		progressBar.hide()
	}
	
	func showMessageError() {
		Alert.show(with: "Atenção", message: "Não foi Possível Carregar notícias. Tente mais tarde", in: self, confirmActionHandler: nil, cancelAction: nil, completion: nil)
	}
	
	func setCategories(categories: [CatergoryView]) {
		self.categories = categories
		tableView.reloadData()
	}
	
	func updateImage(indexPath: IndexPath, url: String, positionImage: Int) {
		let cell = tableView.cellForRow(at: indexPath) as! CategoryItemTableViewCell
		categories[indexPath.section].items[indexPath.row].selecetedImage = positionImage
		let imageUrl = URL(string: url)
		cell.categoryImage.sd_setImage(with: imageUrl, placeholderImage: #imageLiteral(resourceName: "placeholder_no_image"))
		print(positionImage)
	}
	
	func showItemDetail(item: ItemView) {
		performSegue(withIdentifier: "detailSegue", sender: item)
	}
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?){
		if(segue.identifier == "detailSegue"){
			let detailVC = segue.destination as! DetaillViewController
			detailVC.item = sender as! ItemView
		}
	}
}

extension ViewController: UITableViewDataSource, UITableViewDelegate {
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		let item = categories[indexPath.section].items[indexPath.row]
		self.categoryPresenter.selectItem(item: item)
	}
	
	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		return 180.0
	}
	
	func numberOfSections(in tableView: UITableView) -> Int {
		return categories.count
	}
	
	func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
		return categories[section].nameCategory
	}
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return categories[section].items.count
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "categoryIdentifier") as! CategoryItemTableViewCell
		let item = categories[indexPath.section].items[indexPath.row]
		cell.delegate = self
		cell.title.text = item.title
		cell.categoryDescription.text = item.description
		let itemSelected = item.selecetedImage
		let imageUrl = URL(string: item.imagesUrl[itemSelected])
		cell.categoryImage.sd_setImage(with: imageUrl, placeholderImage: #imageLiteral(resourceName: "placeholder_no_image"))
		return cell
	}
}

extension ViewController: ButtonProtocoll {
	func left(cell: UITableViewCell) {
		let index = tableView.indexPath(for: cell)!
		let selectedItem = categories[index.section].items[index.row]
		self.categoryPresenter.replaceImage(index: index, direction: .left, item: selectedItem)
	}
	
	func right(cell: UITableViewCell) {
		let index = tableView.indexPath(for: cell)!
		let selectedItem = categories[index.section].items[index.row]
		self.categoryPresenter.replaceImage(index: index, direction: .right, item: selectedItem)
	}
}
