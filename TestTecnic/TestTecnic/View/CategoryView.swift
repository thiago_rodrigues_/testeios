//
//  CategoryView.swift
//  TestTecnic
//
//  Created by Thiago Rodrigues de Souza on 19/06/2018.
//  Copyright © 2018 Thiago Rodrigues de Souza. All rights reserved.
//
import UIKit

struct CatergoryView {
	let nameCategory: String
	var items: Array<ItemView>
}

struct ItemView {
	let title: String
	let description: String
	let imagesUrl: Array<String>
	var selecetedImage: Int
	
	mutating func selectImage(position: Int){
		selecetedImage = position
	}
}
