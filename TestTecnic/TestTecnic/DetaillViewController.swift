//
//  DetaillViewController.swift
//  TestTecnic
//
//  Created by Thiago Rodrigues de Souza on 22/06/2018.
//  Copyright © 2018 Thiago Rodrigues de Souza. All rights reserved.
//
import UIKit
import SDWebImage

class DetaillViewController: UIViewController {
	
	@IBOutlet weak var iconStar: UIButton!
	@IBOutlet weak var itemImage: UIImageView!
	@IBOutlet weak var itemTitle: UILabel!
	@IBOutlet weak var itemText: UITextView!
	
	var item: ItemView!
	let progressBar = ProgressBarDisplay()
	private let detaillPresenter = DetaillPresenter(service: CategoryService())

	override func viewDidLoad() {
        super.viewDidLoad()
		self.detaillPresenter.attachView(view: self)
		detaillPresenter.setup(item: item)
	}
	
	@IBAction func back(_ sender: UIButton) {
		self.dismiss(animated: true, completion: nil)
	}
	
	@IBAction func favorite(_ sender: UIButton) {
		detaillPresenter.favorite(item: item)
	}
}

extension DetaillViewController: DetailView {
	
	func startLoading() {
		progressBar.show(message: "Carregando...", indicator: true, in: view)
	}
	
	func finishLoading() {
		progressBar.hide()
	}
	
	func setItem(item: ItemView) {
		let url = URL(string: item.imagesUrl[item.selecetedImage])
		itemTitle.text = item.title
		itemText.text = item.description
		itemImage.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "placeholder_no_image"))
	}
	
	func renderStatus(status: Bool) {
		if(status){
			self.iconStar.setImage(#imageLiteral(resourceName: "star"), for:.normal)
			self.iconStar.isUserInteractionEnabled = false
		}else{
			self.iconStar.setImage(#imageLiteral(resourceName: "ic_star"), for: .normal)
		}
	}
}
