//
//  FavoritesViewController.swift
//  TestTecnic
//
//  Created by Thiago Rodrigues de Souza on 22/06/2018.
//  Copyright © 2018 Thiago Rodrigues de Souza. All rights reserved.
//

import UIKit
import CoreData

class FavoritesViewController: UIViewController {
	
	@IBOutlet weak var tableView: UITableView!
	let progressBar = ProgressBarDisplay()
	var favoritedItens = Array<FavoriteItem>()
	private let favoritesPresenter = FavoritePresenter(categorieService: CategoryService())
	
    override func viewDidLoad() {
        super.viewDidLoad()
		tableView.delegate = self
		tableView.dataSource = self
		self.favoritesPresenter.attachView(view: self)
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		self.favoritesPresenter.setup()
	}
	
}

extension FavoritesViewController: FavoritesView {
	
	func startLoading() {
		progressBar.show(message: "Carregando...", indicator: true, in: view)
	}
	
	func finishLoading() {
		progressBar.hide()
	}
	
	func loadingFavorites(favoritedItens: Array<FavoriteItem>) {
		self.favoritedItens = favoritedItens
		tableView.reloadData()
	}
	
	func favoriteIsEmpty() {
		Alert.showDefault(title: "Atenção", message: "Não há itens favoritados", in: self, confirmActionHandler: nil, completion: nil)
	}

	func showFavoriteItemDetail(favoriteItem : FavoriteItem) {
		performSegue(withIdentifier: "detailFavoriteSegue", sender: favoriteItem)
	}
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if(segue.identifier == "detailFavoriteSegue") {
			let vc = segue.destination as! DetailFavoriteViewController
			vc.itemFavorited = sender as? FavoriteItem
		}
	}
}

extension FavoritesViewController: UITableViewDataSource, UITableViewDelegate{
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		let item = favoritedItens[indexPath.row]
		self.favoritesPresenter.selectItem(item: item)
	}
	
	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		return 180.0
	}
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return favoritedItens.count
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "categoryIdentifier") as! CategoryItemTableViewCell
		let item = favoritedItens[indexPath.row]
		cell.title.text = item.title
		cell.categoryDescription.text = item.description
		let imageUrl = URL(string: item.imageUrl)
		cell.categoryImage.sd_setImage(with: imageUrl, placeholderImage: #imageLiteral(resourceName: "placeholder_no_image"))
		return cell
	}
}
