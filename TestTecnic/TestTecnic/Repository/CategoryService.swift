//
//  CategoryService.swift
//  TestTecnic
//
//  Created by Thiago Rodrigues de Souza on 19/06/2018.
//  Copyright © 2018 Thiago Rodrigues de Souza. All rights reserved.
//
import Foundation
import Alamofire
import CoreData

class CategoryService {
	
	let appDelegate = UIApplication.shared.delegate as! AppDelegate
	
	func listCategories(completion: @escaping (([Category], _ error: Error?) ->())){
		let endpoint = "https://cdn.joyjet.com/tech-interview/mobile-test-one.json"
		Alamofire.request(endpoint, encoding: JSONEncoding.default).responseJSON { response in
			guard let error = response.error else{
				if let result = response.result.value {
					let jsonArray = result as! Array<Dictionary<String, Any>>
					let categories = jsonArray.compactMap{Category(dict: $0)}
					return completion(categories, nil)
				}else{
					return completion([], NSError(domain: "Failed Parse JSON", code: 55, userInfo: nil))
				}
			}
			completion([], error)
		}
	}
	
	func persistItem(title: String, description: String, imageUrl: String) -> Bool {
		let context = appDelegate.persistentContainer.viewContext
		let item = ItemData(context: context)
		item.title = title
		item.item_description = description
		item.imageUrl = imageUrl
		do {
			try context.save()
			return true
		} catch {
			print("Failed saving")
			return false
		}
	}
	
	func listItem()-> Array<ItemData> {
		let context = appDelegate.persistentContainer.viewContext
		let request = NSFetchRequest<NSFetchRequestResult>(entityName: "ItemData")
		request.returnsObjectsAsFaults = false
		do {
			let result = try context.fetch(request)
			return result as! Array<ItemData>
		} catch {
			print("Failed")
		}
		return []
	}
	
}
