//
//  CategoryPresenterMock.swift
//  TestTecnic
//
//  Created by Thiago Rodrigues de Souza on 22/06/2018.
//  Copyright © 2018 Thiago Rodrigues de Souza. All rights reserved.
//

import Foundation

class MainViewMock: NSObject, MainView {
	
	var isStartLoding = false
	var isFinishLoading = false
	var isShowMessaged = false
	var isSetCategories = false
	var isUpdateImageCalled = false
	var isShowDetail = false
	
	func startLoading() {
		self.isStartLoding = true
		
	}
	
	func finishLoading() {
		self.isFinishLoading = true
	}
	
	func showMessageError() {
		isShowMessaged = true
	}
	
	func setCategories(categories: [CatergoryView]) {
		isSetCategories = true
	}
	
	func updateImage(indexPath: IndexPath, url: String, positionImage: Int) {
		isUpdateImageCalled = true
	}
	
	func showItemDetail(item: ItemView) {
		isShowDetail = true
	}
	
}
