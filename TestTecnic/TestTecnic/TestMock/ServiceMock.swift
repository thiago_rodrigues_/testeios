//
//  ServiceMock.swift
//  TestTecnic
//
//  Created by Thiago Rodrigues de Souza on 22/06/2018.
//  Copyright © 2018 Thiago Rodrigues de Souza. All rights reserved.
//

import Foundation

class ServiceMock: CategoryService {
	
	private let categories: Array<Category>
	
	init(categories: Array<Category>) {
		self.categories = categories
	}
	
	override func listCategories(completion: @escaping (([Category], Error?) -> ())) {
		if(categories.isEmpty){
			return completion([], NSError(domain: "Failed Parse JSON", code: 55, userInfo: nil))
		}else{
			return completion(categories, nil)
		}
	}
	
}
