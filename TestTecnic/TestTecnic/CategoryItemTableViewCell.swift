//
//  CategoryTableViewCell.swift
//  TestTecnic
//
//  Created by Thiago Rodrigues de Souza on 20/06/2018.
//  Copyright © 2018 Thiago Rodrigues de Souza. All rights reserved.
//
import UIKit

protocol ButtonProtocoll {
	func left(cell: UITableViewCell)
	func right(cell: UITableViewCell)
}

class CategoryItemTableViewCell: UITableViewCell {

	@IBOutlet weak var title: UILabel!
	@IBOutlet weak var categoryDescription: UILabel!
	@IBOutlet weak var categoryImage: UIImageView!
	
	var delegate: ButtonProtocoll?
	
	override func awakeFromNib() {
        super.awakeFromNib()
	}
	
	@IBAction func leftAction(_ sender: UIButton) {
		delegate?.left(cell: self)
	}
	
	@IBAction func rightAction(_ sender: UIButton) {
		delegate?.right(cell: self)
	}

}
