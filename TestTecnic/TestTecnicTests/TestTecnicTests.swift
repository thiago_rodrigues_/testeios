//
//  TestTecnicTests.swift
//  TestTecnicTests
//
//  Created by Thiago Rodrigues de Souza on 19/06/2018.
//  Copyright © 2018 Thiago Rodrigues de Souza. All rights reserved.
//

import XCTest

@testable import TestTecnic

class TestTecnicTests: XCTestCase {
	
	let emptyServiceMock = ServiceMock(categories: [])

    override func setUp() {
        super.setUp()
    }
	
    func testExample() {
		let mainMock = MainViewMock()
		let categorypresenterTest = CategoryPresenter(service: emptyServiceMock)
		categorypresenterTest.attachView(view: mainMock)
		categorypresenterTest.setupView()
		XCTAssertFalse(mainMock.isSetCategories)
	}
    
	
}
